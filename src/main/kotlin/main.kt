import java.io.File


fun main(args: Array<String>) {
    val wordLengthDistribution1 = createWordLengthRandomVariable(args[0])
    val wordLengthDistribution2Without15 = createWordLengthRandomVariable(args[1])
    val wordLengthDistribution2 = RandomVariable(wordLengthDistribution2Without15.values + Value(15.0, 0))

    val charDistribution1 = createCharRandomVariable(args[0])
    val charDistribution2 = createCharRandomVariable(args[1])

    println("Nahodna velicina delek slov v 1. textu = $wordLengthDistribution1")
    println("Nahodna velicina delek slov v 2. textu = $wordLengthDistribution2")
    println("Rozdeleni pismen v 1. textu = size: ${charDistribution1.size}, values: ${charDistribution1.valuesToString { it.toInt().toChar().toString() }}")
    println("Rozdeleni pismen v 2 . textu = size: ${charDistribution2.size}, values: ${charDistribution2.valuesToString { it.toInt().toChar().toString() }}")


    println("Hodnota testovane statistiky v testu nezavislosti v kontingencnich tabulkach (ukol 3 a 5):")
    println("Rozdeleni pismen v textech")
    println("Metoda 1: ${testNezavislostiVKontingencniTabulce_HodnotaStatistiky_Metoda1(listOf(charDistribution1, charDistribution2))}")
    println("Metoda 2: ${testNezavislostiVKontingencniTabulce_HodnotaStatistiky_Metoda2(listOf(charDistribution1, charDistribution2))}")
    val PEARSON_005_25 = 14.6
    val PEARSON_095_25 = 37.7
    println("Rozdeleni delek slov v textech")
    println("Metoda 1: ${testNezavislostiVKontingencniTabulce_HodnotaStatistiky_Metoda1(listOf(wordLengthDistribution1, wordLengthDistribution2))}")
    println("Metoda 2: ${testNezavislostiVKontingencniTabulce_HodnotaStatistiky_Metoda2(listOf(wordLengthDistribution1, wordLengthDistribution2))}")
    val PEARSON_005_14 = 6.57
    val PEARSON_095_14 = 23.7

    println("Hodnota testovane statistiky v dvouvyberovem t-testu (ukol 4):")
    println("Pri stejnem rozptylu: ${dvouvyberovyTTestPriStejnemRozptylu(wordLengthDistribution1, wordLengthDistribution2Without15)}")
    println("Pri ruznem rozptylu: ${dvouvyberovyTTestPriRuznemRozptylu(wordLengthDistribution1, wordLengthDistribution2Without15)}")
}

fun dvouvyberovyTTestPriStejnemRozptylu(x: RandomVariable, y: RandomVariable): Double {
    val Xn = x.E
    val Ym = y.E
    val n = x.n
    val m = y.n
    val s12 = Math.sqrt((((n -1) * x.s2n) + (m -1) * y.s2n) / (n + m -2))
    return (Xn - Ym) / s12 * Math.sqrt(n * m / (n + m).toDouble())
}


fun dvouvyberovyTTestPriRuznemRozptylu(x: RandomVariable, y: RandomVariable): Double {
    val Xn = x.E
    val Ym = y.E
    val n = x.n
    val m = y.n
    val sd = Math.sqrt(x.s2n / n + y.s2n / m)
    val nd = Math.pow(sd, 4.0) / Math.pow(x.s2n / n, 2.0) / (n -1) + Math.pow(y.s2n / m, 2.0) / (m -1)
    println("ND = $nd")
    return (Xn - Ym) / sd
}

fun testNezavislostiVKontingencniTabulce_HodnotaStatistiky_Metoda1(variables: List<RandomVariable>): Double {
    val n = variables.map { it.n }.sum().toDouble()
    return (0 .. variables.size -1).map { i ->
        (0 .. variables[0].size -1).map { j ->
            val Nij = variables[i][j].count
            val Ni_ = variables[i].n
            val N_j = variables.map { it[j].count }.sum()

            val sub = (Ni_ * N_j / n)

            Math.pow(Nij - sub, 2.0) / sub
        }.sum()
    }.sum()
}

fun testNezavislostiVKontingencniTabulce_HodnotaStatistiky_Metoda2(variables: List<RandomVariable>): Double {
    val n = variables.map { it.n }.sum().toDouble()
    return (0 .. variables.size -1).map { i ->
        (0 .. variables[0].size -1).map { j ->
            val Nij = variables[i][j].count.toDouble()
            val Ni_ = variables[i].n
            val N_j = variables.map { it[j].count }.sum()

            Math.pow(Nij, 2.0) / Ni_ / N_j
        }.sum()
    }.sum() * n -n
}

fun createWordLengthRandomVariable(fileName: String) =
    RandomVariable(fileName
        .getText()
        .split(" ")
        .map { it.length }
        .fold(emptyMap(), { acc: Map<Int, Int>, i: Int ->
            acc + Pair(i, acc.getOrDefault(i, 0) + 1)
        })
        .map { Value(it.key.toDouble(), it.value) }
        .sortedBy { it.value }
    )


fun createCharRandomVariable(fileName: String): RandomVariable =
    RandomVariable(fileName
        .getText()
        .fold(emptyMap(), { acc: Map<Char, Int>, c: Char ->
            if (c == ' ') acc
            else acc + Pair(c, acc.getOrDefault(c, 0) + 1)
        })
        .map { Value(it.key.toInt().toDouble(), it.value) }
        .sortedBy { it.value }
    )

fun String.getText(): String = File(this)
        .readText(Charsets.UTF_8)
        .split("\n")[1]